export HISTSIZE=5000
export HISTFILE='~/History/ksh_histfile'
#set the suspend character to ^Z (control-z):
stty susp '^z'
#  don't let control-d logout
set -o ignoreeof
alias aa='ls -Ih --color'
alias zz='ls -Al --color'
alias qq='gnuls --color'
alias lsl='ls -Al --color'
alias lss='ls -Ih --color'
PS1='! KSH: $PWD $ '
VISUAL=nvim
EDITOR=nvim
set -o noclobber

export CLICOLOR
export COLORFGBG="15;0"
export COLORTERM="truecolor"
export TERM="xterm-256color"

set -o emacs
